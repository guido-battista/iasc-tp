# IASC-TP

**Instalación**

Ejecutar el comando NPM INSTALL en las carpetas:
- Master
- Node
- commons
- ClientePrueba

**Configuracion**

Editar el Json \commons\masters.js indicando las ip y puertos de los Master del sistema.

**Ejecutar un Master**

- Ingresar a la carpeta Master
- Editar el archivo datosMaster.js, modificando la línea **var ip = "localhost"** con la ip del host.
- Ejecutar el comando: **node main.js (port)** [Ej: node main.js 8080]


**Ejecutar un Nodo**

- Ingresar a la carpeta Node
- Editar el archivo datosNodo.js, modificando la línea **var ip = "localhost"** con la ip del host.
- Ejecutar el comando: **node main.js (port) (nombre)** [Ej: node main.js 8000 A]

**Ejecutar un cliente de prueba**

- Ingresar a la carpeta ClientePrueba
- Ejecutar el comando: **node main.js (port)** [Ej: node main.js 3000]
- Ingresar en algún navegador a la ip y puerto del cliente

**Crear una cola**

- Ejecutar un Post a alguno de los nodoso a la url http://[ipNodo]:[puertoNodo]/crearCola indicando en el body

{
	"nombre":*nombre de cola*,
	"modalidad": *R=RoundRobin, T=Todos*
}

Ejemplo:

{
	"nombre": "Cola1",
	"modalidad": "T"
}

**Borrar una cola**
- Ejecutar un Delete a alguno de los nodoso a la url http://[ipNodo]:[puertoNodo]/cola/{nombreCola}



