var express = require('express');
var app = express();

var port = parseInt(process.argv[2], 10);

app.use('/static', express.static(__dirname + '/public'));
app.use('/modules', express.static(__dirname + '/Client'));

app.get('/', function (req, res) {
    res.sendFile('index.html', { root : __dirname});
});

var listener = app.listen(port, function () {
  console.log('Example app listening on port ' + listener.address().port);
});