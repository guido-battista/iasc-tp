$(document).ready(function () {

  console.log("Script cargado");

  var modalidad;
  var nombreCola;
  var gestor;

  $("#modo").change(function () {
    var modo = $("#modo>option:selected").val();
    if (modo == "consumidor") {
      $("#consumo").css("visibility", "visible");
    } else {
      $("#consumo").css("visibility", "hidden");
    }
  });

  $("#consumidor").hide();
  $("#productor").hide();

  $("#suscribe").click(function () {
    if ($("#nombreCola").val() == "") {
      alert("Tenes que ingresar una cola");
      return;
    }
    $("#desconectar").show();

    nombreCola = $("#nombreCola").val();
    var modo = $("#modo>option:selected").val();
    console.log("Modo seleccionado");
    console.log(modo);

    var modoConsumo = $('input:radio[name=modoConsumo]:checked').val();
    console.log("modo consumo");
    console.log(modoConsumo);

    //Se consulta el node de la cola
    gestor = new Gestor();
    gestor.obtenerCola(nombreCola, modo)
      .then((colaObtenida) => {
        cola = colaObtenida;
        cambiarPantalla(modo);

        gestor.onColaBorrada = colaBorrada;

        if (modo == "consumidor") {
          gestor.consumirMensaje = agregarMensaje;
          if (modoConsumo == "T") {
            console.log("Consumir transaccionalmente");
            gestor.consumirTransaccionalmente()
          }
        }
      })
      .catch((error) => alert("Hubo un error al buscar al obtener la cola: " + error));
  });

  colaBorrada = () => {
    alert("Se borró la cola...");
    desconectar();
  }

  cambiarPantalla = (modo) => {

    $("#principal").hide();

    if (modo == "productor") {
      $("#colaHeaderProductor").text(nombreCola);
      $("#productor").show();
    } else {
      $("#colaHeaderConsumidor").text(nombreCola);
      var modoConsumo = $('input:radio[name=modoConsumo]:checked').val();
      $("#consumoTransaccionalText").text(modoConsumo == "T" ? "Transaccionalmente" : "No Transaccionalmente");
      $("#consumidor").show();
    }
  }

  $("#desconectar").click(function () {
    desconectar();
  });

  desconectar = () => {
    $("#consumidor").hide();
    $("#productor").hide();
    $("#principal").show();
    $("#mensajes").empty();
    $("#desconectar").hide();
    gestor.cerrarConexion();
  }

  // $("#backP").click(function(){
  //   $("#productor").hide();
  //   $("#principal").show();
  //   gestor.cerrarConexion();
  // });

  $("#producir").click(function () {
    //texto = $("#mensaje").val();
    var number = parseInt($("#mensaje").val(), 10);

    gestor.producirMensaje({ texto: number.toString() });
    number = number + 1;
    $("#mensaje").val(number.toString());
    // alert("Mensaje producido correctamente");
  });

  agregarMensaje = async (mensaje) => {
    console.log("Consumir mensaje nuevo");
    var tiempo = $("#tiempo").val();
    if (tiempo == "")
      tiempo == 1;
    await timeout(tiempo * 1000);
    $("#mensajes").append('<li class="list-group-item"> ' + mensaje.texto + '</li>');
  }

  function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
});