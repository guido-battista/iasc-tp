class Gestor {

    constructor() {
        //Defino atributos
        this.socket = null;
        this.nombreCola = null;
        this.idRecibido = false;
        //this.ackPendient = null;
        //Lo arranco en true para probar
        this.consumoTransaccional = false;
        this.backup = null;
        this.modo = null;
        this.activo = null;
        this.url = "localhost:8001";

        //Defino metodos de instancia
        this.obtenerCola = async (nombreCola, modo, reconexion) => {

            //Se consulta el nodo que tiene la cola
            var res = await axios.get("http://" + this.url + "/cola/" + nombreCola);
            console.log("Respuesta");
            console.log(res.data);

            if (res.data.retorno == 0) {
                //Se guadran los datos
                this.nombreCola = nombreCola;
                this.modo = modo;

                await this.conectarNodo(res.data.activo, false);

            } else {
                throw res.data.retorno
            }
        };

        this.producirMensaje = (mensaje) => {
            this.socket.emit('mensaje_nuevo', { id: this.id, cola: this.nombreCola, mensaje })
        }

        this.consumirMensaje = (data) => {
            console.log("Consumir mensaje viejo");
        };

        this.onColaBorrada = () => {
            console.log("Se borró la cola");
        };

        this.cerrarConexion = () => {

            console.log("Se desconecta el socket");
            // if (this.ack != null) {
            //     this.ack();
            // }
            this.socket.disconnect(true);
        }

        this.consumirTransaccionalmente = () => {
            this.consumoTransaccional = true;
        }

        this.conectarABackup = async () => {

        console.log("Conectar a backup");
        console.log(this.nombreCola);
        //Se consulta el nodo que tiene la cola
        var res = await axios.get("http://" + this.url + "/cola/" + this.nombreCola);

        console.log("Respuesta de conectar a backup");
        console.log(res.data);

        if (res.data.retorno == 0) {
            //Se revisa que la IP y puerto sean distintas al del nodo que se cayó
            if (res.data.activo.ip != this.activo.ip || res.data.activo.port != this.activo.port) {

                await this.conectarNodo(res.data.activo, true);

            } else {
                //Se vuelve a consultar en 5 segundo
                setTimeout(this.conectarABackup, 5000);
            }
        } else {
            //Se vuelve a consultar en 5 segundo
            setTimeout(this.conectarABackup, 5000);
        }
        //----------------
        }
    };

    async conectarNodo(nodoActivo, reconexion) {

        //Se guarda las direcciones de los nodos
        this.activo = nodoActivo;
        // this.backup = res.data.backup;

        console.log("Nodo a conectarse");
        console.log(this.activo);

        //Se crea un socket con ese nodo
        this.socket = await io.connect('http://' + nodoActivo.ip + ":" + nodoActivo.port, { 'forceNew': true });

        //Se informa la nueva suscripcion
        await this.socket.emit('nueva_suscripcion', { cola: this.nombreCola, modalidad: this.modo, id: reconexion ? this.id : null });

        //Se recibe el ID
        this.socket.on('conexion_aceptada', (data) => {
            console.log("Conexión aceptada");
            this.id = data.id;
        });

        //Se atiende el evento de mensaje recibido
        this.socket.on('consumir_mensaje', async (data, ack) => {
            //console.log("Booleano");
            //console.log(this.consumirTransaccionalmente);
            if (this.consumoTransaccional) {
                console.log("Se consume transaccionalmente");
                //this.ack = ack;
                await this.consumirMensaje(data);
                ack();
                //this.ack = null;
            } else {
                console.log("Se consume no transaccionalmente");
                ack();
                await this.consumirMensaje(data);
            }

        });

        this.socket.on('cola_borrada', () => {
            console.log("Se recibe el mensaje de que se borró la cola");
            this.socket.disconnect();
            console.log("Se ejecuta la funcion de cola borrada");
            this.onColaBorrada();
        })

        //Se atiende el evento de desconexión
        this.socket.on('disconnect', (reason) => {
            console.log("Se desconectó el nodo");
            console.log(reason);
            if (reason != 'io client disconnect') {
                console.log("Se desconectó el servidor");
                this.cerrarConexion();
                this.conectarABackup();
                // this.obtenerCola(nombreCola, modo);
            }
        });
    }
};
