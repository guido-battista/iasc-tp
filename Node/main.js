//Obtengo los datos de este nodo
var datosNodo = { ip, port, nombre } = require('./datosNodo');

console.log("Nodo: " + nombre);

//Require del estado del nodo
var Estado = require('./lib/model/NodeEstado');

//Conexión con Master
var MasterConexion = require('./lib/components/MasterConexion');

//Servidor
var Servidor = require('./lib/components/Servidor');

//Se levanta la conexion con master, y cuando el Master esté conectado, se levanta el servidor
console.log(MasterConexion.connect);
MasterConexion.connect(false, Servidor.up);
