var ip = "localhost"

//De momento recibo por parámetro puerto y nombre del nodo
var port = parseInt(process.argv[2], 10);
var nombre = process.argv[3];

module.exports = {
    ip,
    port,
    nombre
}