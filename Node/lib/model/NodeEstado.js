var Estado = {
    nodos: [],
    conexionesNodos: [],
    conexionesProductores: [],
    conexionesConsumidores: [],
    colas: [],
    colasBack: [],
    //Se guarda como lista para no perder el valor
    socketMaster: []
}

module.exports.nodos = Estado.nodos;
module.exports.conexionesNodos = Estado.conexionesNodos;
module.exports.conexionesProductores = Estado.conexionesProductores;
module.exports.conexionesConsumidores = Estado.conexionesConsumidores;
module.exports.colas = Estado.colas;
module.exports.colasBack = Estado.colasBack;

//También se podría hacer mejor
module.exports.getSocketMaster = () => {
    return Estado.socketMaster[0];
}

module.exports.addColas = (cola) => Estado.colas.push(cola);
module.exports.addConexionesNodos = (conexion) => Estado.conexionesNodos.push(conexion);
module.exports.addConexionesProductores = (conexion) => Estado.conexionesProductores.push(conexion);
module.exports.addConexionesConsumidores = (conexion) => Estado.conexionesConsumidores.push(conexion);
module.exports.addNodo = (nodo) => Estado.nodos.push(nodo);
module.exports.addColasBack = (cola) => Estado.colasBack.push(cola);

//Sacar
module.exports.Estado = Estado;

//Habría que ver si esto se puede hacer de una forma más prolija...
module.exports.setSocketMaster = (socket) => {
    if (Estado.socketMaster.length > 0) {
        Estado.socketMaster.pop();
    };
    Estado.socketMaster.push(socket)
};

module.exports.existeCola = (cola) => {
    var existeCola = false;
    Estado.nodos.forEach((nodo) => {
        console.log(nodo);
        console.log(nodo.colas);
        if (nodo.colas.includes(cola)) {
            existeCola = true;
        }
    })
    return existeCola;
}


module.exports.replaceNodos = (nuevosNodos) => Estado.nodos.splice(0, Estado.nodos.length, ...nuevosNodos);

module.exports.addConsumidorEnCola = (nombreCola, consumidorId) => {
    const cola = Estado.colas.find((cola) => cola.nombre == nombreCola);
    cola.consumidoresId.push(consumidorId);
}

module.exports.findColaByNombre = (nombreCola) => Estado.colas.find((cola) => cola.nombre == nombreCola);

module.exports.getConexionYTipoBySocket = (socket) => {
    //Se verifica si la conexión es de un productor
    var index = Estado.conexionesProductores.map((conexion) => conexion.socket).indexOf(socket);
    if (index > -1) {
        return { conexion: Estado.conexionesProductores[index], tipo: "Productor" };
    } else {
        //Se verifica si la conexión es de un consumidor
        index = Estado.conexionesConsumidores.map((conexion) => conexion.socket).indexOf(socket);
        console.log("Index de consumidores: " + index.toString());
        if (index > -1) {
            return { conexion: Estado.conexionesConsumidores[index], tipo: "Consumidor" };
        } else {
            //Se verifica si la conexión es de un nodo
            index = Estado.conexionesNodos.map((conexion) => conexion.socket).indexOf(socket);
            if (index > -1) {
                return { conexion: Estado.conexionesConsumidores[index], tipo: "Nodo"};
            }
        }
    };
    return null;
}

module.exports.removerConexionConsumidor = (conexion) => {
    index = Estado.conexionesConsumidores.indexOf(conexion);
    Estado.conexionesConsumidores.splice(index, 1);
};

module.exports.removerConexionProductor = (conexion) => {
    index = Estado.conexionesProductores.indexOf(conexion);
    Estado.conexionesConsumidores.splice(index, 1);
};

module.exports.removerConexionNodo = (conexion) => {
    index = Estado.conexionesNodos.indexOf(conexion);
    Estado.conexionesConsumidores.splice(index, 1);
};

module.exports.RemoveBackup = (cola) => {
    var index = Estado.colasBack.indexOf(cola);
    Estado.colasBack.splice(index, 1);
};

module.exports.RemoveCola = (cola) => {
    var index = Estado.colas.indexOf(cola);
    Estado.colas.splice(index, 1);
}

module.exports.RemoveConsumidorFromCola = (cola, idBorrar) => {
    cola.consumidoresId = cola.consumidoresId.filter((id) => { return id != idBorrar });
}

module.exports.RemoveConsumidorFromMensaje = (mensaje, idBorrar) => {
    mensaje.consumidores = mensaje.consumidores.filter((consumidor) => consumidor != idBorrar);
};
