class ConexionNodo {
    constructor(socket, nombre, ip, port) { 
        this.socket = socket; 
        this.nombre = nombre; 
        this.ip = ip;
        this.port = port 
    };
}

module.exports = ConexionNodo;