class Mensaje {
  constructor(data, consumidoresId) {
    this.data = data,
    this.consumidores = consumidoresId,
    this.enviado = false
  };
};

module.exports = Mensaje;