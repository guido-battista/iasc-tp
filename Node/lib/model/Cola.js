class Cola {
    constructor(nombre, modalidad) {
        this.nombre = nombre,
        this.modalidad = modalidad,
        this.mensajes = [],
        this.consumidoresId = [],
        this.iDConsumidorActual = "";
        this.quantum = 0,
        this.borrada = false,
        this.consumiendo = false
      };

      agregarMensaje(mensaje) {
          this.mensajes.push(mensaje);
      }

      removerMensaje() {
          this.mensajes.shift();
      }
    
      seleccionarConsumidor(conexionesConsumidores) {
        const quantumTotal = 3;
    
        var ind = conexionesConsumidores.map((conexion) => conexion.id).indexOf(this.iDConsumidorActual);
        
        //Signfica que el consumidor actual se desconecto => se reinicia el quantum y el índice
        if (ind == -1) {
            this.quantum = 1;
            this.iDConsumidorActual = conexionesConsumidores[0].id;
            return conexionesConsumidores[0];
        };
    
        //Si el quantum es menor al maximo - 1 => se suma uno al quantum y se devuelve el consumidor actual 
        if ( this.quantum < quantumTotal ) {
            this.quantum = this.quantum + 1;
            return conexionesConsumidores[ind];
        } else {
            this.quantum = 1;
            if (ind < conexionesConsumidores.length - 1) {
                ind = ind + 1;
            } else {
                ind = 0;
            };
            this.iDConsumidorActual = conexionesConsumidores[ind].id;
            return conexionesConsumidores[ind];;
        };
    };

    cambiarConsumidor(conexionesConsumidores, ultimoConsumidor) {
        console.log("Cambiar consumidor");
        var ind = conexionesConsumidores.indexOf(ultimoConsumidor);
        console.log(ind);
        this.quantum = 0;
        if (ind < conexionesConsumidores.length - 1) {
            ind = ind + 1;
        } else {
            ind = 0;
        };
        this.iDConsumidorActual = conexionesConsumidores[ind].id;
    }
}

module.exports = Cola;