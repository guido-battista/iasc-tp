class ConexionCliente {
    constructor(id, socket, cola) { 
        this.id = id; 
        this.socket = socket;
        this.cola = cola;
    };
}

module.exports = ConexionCliente;