var Estado = require('../model/NodeEstado');
const datosNodo = { ip, port, nombre } = require('../../datosNodo');
const Masters = require('../../../Commons/masters');


conectarConMaster = (reconexion, levantarServidor) => {

    //Master Controller
    // var MasterController = require('../controllers/MasterController');

    //Master Controller
    var ColaController = require('../controllers/ColaController');

    //Nodo Controller
    var NodoController = require('../controllers/NodoController');

    //Se obtiene el master activo
    if (reconexion) {
        Masters.shift();
    };

    var MasterActivo = Masters[0];

    console.log("Master Activo");
    console.log(MasterActivo);

    //Se conecta con el master
    var ioMaster = require('socket.io-client'),
        socketMaster = ioMaster.connect('http://' + MasterActivo.ip + ':' + MasterActivo.port.toString());

    //Cuando la conexión es exitosa, se envía datos del nodo, y se levanta el servidor
    socketMaster.on('connect', function () {
        console.log("Conexión exitosa con Master");
        socketMaster.emit('envio_ip_puerto', { nombre: datosNodo.nombre, ip: datosNodo.ip, port: datosNodo.port, reconexion });
        Estado.setSocketMaster(socketMaster);

        //Si no es una reconexión => se levanta elservidor
        if (!reconexion) {
            levantarServidor();
        }
    });

    //Se recibe el pedido del Master para actualizar el estado (listas)
    socketMaster.on('actualizar_listas', (nuevoEstado, ack) => { NodoController.ActualizarListas(nuevoEstado, ack); });

    //Petición del Master para crear una colo en este nodo
    socketMaster.on('crear_cola', (cola, ack) => ColaController.CrearCola(cola, ack));

    //Petición del Master para crear un backup
    socketMaster.on('crear_backup', (cola, ack) => NodoController.CrearBackup(cola, ack));

    //Se recibe la petición de pasar una cola de backup a activa
    socketMaster.on('activar_back', (data, ack) => ColaController.ActivarBackup(data.cola, data.nodoBack, ack));

    //Se recibe una solicitud para informar a un nodo que genere un backup
    socketMaster.on('solicitar_back_up', (data, ack) => ColaController.SolicitarGenerarNuevoBack(data.cola, data.nodoBack, ack));

    //Se recibe la desconeción de un nodo o una suscripción
    socketMaster.on('disconnect', () => conectarConMaster(true));

    //Se recibe la orden de borrar una cola
    socketMaster.on('borrar_cola', (cola, ack) => ColaController.BorrarCola(cola, ack));

    //Se recibe la orden de borrar un nodo
    socketMaster.on('borrar_back', (cola, ack) => ColaController.BorrarBack(cola, ack));

};

module.exports.connect = conectarConMaster;
