var Estado = require('../model/NodeEstado');
var ColaController = require('../controllers/ColaController');
var NodoController = require('../controllers/NodoController');

module.exports.listen = (socket) => {
    //Se recibe el mensaje de un nuevo nodo
    socket.on('nuevo_nodo', (data) => NodoController.NuevoNodo(socket, data));

    //Se recibe una nueva subscripcion
    socket.on('nueva_suscripcion', (data) => ColaController.NuevaSubscripcion(socket, data));

    //Se recibe un nuevo mensaje
    socket.on('mensaje_nuevo', (data) => ColaController.MensajeNuevo(data))

    //Se recibe actualización de backup
    socket.on('actualizar_bk', (cola) => ColaController.ActualizarBackup(cola));

    //Se recibe la desconeción de un nodo o una suscripción
    socket.on('disconnect', () => ColaController.DesonectarNodo(socket));
}