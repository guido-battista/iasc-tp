var Estado = require('../model/NodeEstado');
const datosNodo = { ip, port, nombre } = require('../../datosNodo');

module.exports.up = () => {
    var express = require('express');
    var app = express();
    const cors = require("cors");
    var server = require('http').Server(app);
    var io = require('socket.io')(server);

    var SocketListener = require('./SocketListener')

    app.use(cors());
    app.use(express.json());

    console.log("Se levanta el servidor");

    var ColaController = require('../controllers/ColaController');
    //var NodoController = require('../controllers/NodoController');

    //Petición para crear una cola
    app.post('/crearCola', (req, res) => ColaController.InformarNuevaCola(req, res));

    //Petición para obtener el nodo que tiene la cola
    app.get('/cola/:nombre', (req, res) => ColaController.GetCola(req, res));

    //Petición para borrar una cola
    app.delete('/cola/:nombre', (req, res) => ColaController.DeleteCola(req, res));

    //Se levanta el servidor para escuchar conexiones de sockets
    server.listen(port, function () {
        //console.log('Listening on port ' + srv.address().port);
        console.log('Servidor corriendo en http://localhost:' + server.address().port.toString());
    });

    //Se recibe una conexion
    io.on('connection', function (socket) {
        console.log('Un cliente se ha conectado');

        //Se escuchan las peticiones del socket
        SocketListener.listen(socket);
    });
};