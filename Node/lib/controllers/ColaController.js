var Estado = require('../model/NodeEstado');
const Cola = require('../model/Cola');
const Mensaje = require('../model/Mensaje');
const ConexionNodo = require('../model/ConexionNodo');
const ConexionCliente = require('../model/ConexionCliente');
const SocketListener = require('../components/SocketListener');
const datosNodo = { ip, port, nombre } = require('../../datosNodo');

const funciones = require('../../../Commons/functions');

module.exports.InformarNuevaCola = (req, res) => {

    //Se verifica que la cola tenga nombre y modalidad
    if (req.body.nombre === null) {
        res.status(400).send("Se debe informar nombre de la cola");
        return;
    };
    if (req.body.modalidad != "T" && req.body.modalidad != "R") {
        res.status(400).send("Modalidad incorrecta o no informada");
        return;
    };
    Estado.getSocketMaster().emit('nueva_cola', { nombre: req.body.nombre, modalidad: req.body.modalidad }, () => res.status(400).send("OK"));
};

module.exports.DeleteCola = (req, res) => {
    console.log("Borrar Cola: " + req.params.nombre);

    if (!req.params.nombre) {
        return res.status(400).send("Nombre de cola no informado");
    };


    if (!Estado.existeCola(req.params.nombre)) {
        console.log("Se sale por error");
        return res.status(400).send("La cola no existe");
    };

    Estado.getSocketMaster().emit('borrar_cola', req.params.nombre, (status, ret) => res.status(status).send(ret));
}

module.exports.GetCola = (req, res) => {
    console.log("Get Cola: " + req.params.nombre);

    if (Estado.nodos.length == 0) {
        res.status(200).send({ retorno: 10 });
        return;
    }

    var nodo = Estado.nodos.find(function (nodo) {
        return nodo.colas.includes(req.params.nombre);
    });

    var back = Estado.nodos.find(function (nodo) {
        return nodo.backups.includes(req.params.nombre);
    });

    if (nodo != null && back != null) {
        res.status(200).send({ retorno: 0, activo: { ip: nodo.ip, port: nodo.port }, backup: { ip: back.ip, port: back.port } });
    } else {
        res.status(200).send({ retorno: 100 });
    }
};

module.exports.NuevaSubscripcion = (socket, data) => {

    console.log("Nueva subscrición");
    console.log(data);

    var IdGenerator = require('../IdGenerator');
    var id = "";
    var conexionRestaurada = false;

    //Si no se recibe un ID, es porque es la primer conexion 
    if (!data.id) {
        //Se genera un ID nuevo para el cliente
        id = IdGenerator.generate();
    } else {
        conexionRestaurada = true;
        id = data.id;
        console.log("Se recibió la conexión de un backup");
    }

    //Se informa que se aceptó la conexión
    socket.emit('conexion_aceptada', { id });

    var conexion = new ConexionCliente(id, socket, data.cola);

    //Se guarda en la cola correspondiente
    switch (data.modalidad) {
        case 'productor':
            //Se actualiza la lista de productores
            Estado.addConexionesProductores(conexion);
            break;
        case 'consumidor': {

            console.log("Se agrega la conexion del consumidor");
            //Se actualiza la lista de consumidores
            Estado.addConexionesConsumidores(conexion);

            if (!conexionRestaurada) {
                //Se actualiza la lista de consumidores de la cola
                Estado.addConsumidorEnCola(data.cola, id);

                //Se actualizaa el backup
                actualizarColaBack(data.cola);
            }
        };
            break;
    };

    //Se busca la cola en el estado
    console.log("Cola a buscar");
    console.log(data.cola);
    console.log(Estado.colas);
    var cola = Estado.colas.find((cola) => cola.nombre == data.cola);
    // Si:
    // - La cola tiene modalidad Round-Robin
    // - Quien se suscribió es un consumidor
    // - La cola tiene mensajes pendientes 
    // - No se están consumiendo los mensajes de la cola
    if (cola.modalidad == "R" && data.modalidad == "consumidor" && cola.mensajes.length > 0 && !cola.consumiendo) {
        //Se consumen los mensajes de la cola
        consumirMensaje(cola);
    }
};

module.exports.MensajeNuevo = (data) => {
    console.log("Se recibe el mensaje nuevo para la cola: " + data.cola);

    //Se busca la cola en el estado
    var cola = Estado.findColaByNombre(data.cola);;

    //Si la cola tiene modalidad de TODOS
    if (cola.modalidad == "T") {
        if (cola.consumidoresId.length == 0) {
            console.log("No se encola el mensaje porque no hay consumidores");
            return;
        }
    };

    //Se revisa si la cola no se marcó como borrada
    if (cola.borrada) {
        //Si fue borrada, se vuelve
        return;
    };

    //Se crea el mensaje nuevo
    var mensaje = new Mensaje(data.mensaje, Estado.conexionesConsumidores.filter((consumidor) => consumidor.cola == data.cola).map((conexion) => { return conexion.id }));

    //Se agrega el mensaje en la cola
    cola.agregarMensaje(mensaje);

    //Se actualizaa el backup
    actualizarColaBack(cola.nombre);

    //Si el que se agregó es el único mensaje => se consume
    if (cola.mensajes.length == 1) {
        consumirMensaje(cola);
    }
};

module.exports.ActualizarBackup = (cola) => {
    console.log("Se actualiza un back up: " + cola.nombre);

    //Se crea un objeto Cola para guardar en el backup
    var colaAgregar = new Cola(cola.nombre, cola.modalidad);
    colaAgregar.consumidoresId = cola.consumidoresId;
    colaAgregar.mensajes = cola.mensajes;

    //Se busca el backup que se debe actualizar
    var index = Estado.colasBack.findIndex((colaBack) => colaBack.nombre == cola.nombre);
    if (index >= 0) {
        //Se existe, se lo reemplaza
        Estado.colasBack[index] = colaAgregar;
    } else {
        //Si no existe, se agrega a la lista
        Estado.colasBack.push(colaAgregar);
    };
};

module.exports.CrearCola = async (cola, ack) => {
    console.log("Crear cola: " + cola.nombre);
    //Se instancia una nueva Cola
    var nuevaCola = new Cola(cola.nombre, cola.modalidad);

    //Se agrega la cola en el estado
    Estado.addColas(nuevaCola);

    //Se busca en el estado el nodo que tiene el backup
    var nodoBack = Estado.nodos.find((nodo) => { return nodo.backups.includes(nuevaCola.nombre) });

    //Se conecta con el nodo que tiene el backup
    await conectarConBackup(nodoBack);

    //Confirmo que terminó el proceso
    ack();

};

conectarConBackup = (nodoBack) => {
    return new Promise((resolve) => {
        console.log("Conectar con backup");
        //Revisa si existe una conexión con el nodo que tiene el backup
        var conexion = Estado.conexionesNodos.find((conexion) => { return conexion.nombre == nodoBack.nombre })

        //Si no existe, se crea una
        if (!conexion) {

            console.log("Se crea una conexión nueva")
            //Se crea el socket con el nodo que tiene el backup
            var ioNodoBack = require('socket.io-client'),
                socketNodoBack = ioNodoBack.connect('http://' + nodoBack.ip + ":" + nodoBack.port.toString());

            //Se aplican los listeners sobre el socket
            SocketListener.listen(socketNodoBack);

            //Cuando la conexión es exitosa, se envía datos del nodo
            socketNodoBack.on('connect', async function () {
                socketNodoBack.emit('nuevo_nodo', { nombre: datosNodo.nombre, ip: datosNodo.ip, port: datosNodo.port });
                var conexionNueva = new ConexionNodo(socketNodoBack, nodoBack.nombre, nodoBack.ip, nodoBack.port)
                Estado.addConexionesNodos(conexionNueva);

                //Se retorna la conexión creada
                resolve(conexionNueva);
            });
        } else {
            return resolve(conexion);
        };
    })
};

consumirMensaje = async (cola) => {
    console.log("Se consume un mensaje de la cola");

    //Si la cola fue borrada, se vuelve sin consumir el mensaje
    if (cola.borrada) {
        return;
    };

    //Si la cola no tiene mensajes, no se sigue procesando
    if (cola.mensajes.length == 0) {
        console.log("Se vuelve porque no hay mensajes...");

        //Se marca en el estado que no se están consumiendo mensajes de la cola
        cola.consumiendo = false;
        return;
    };

    //Se marca en el estado que se están consumiendo mensajes de la cola
    cola.consumiendo = true;

    var mensaje = cola.mensajes[0];

    switch (cola.modalidad) {
        case "T": {
            //Se determinan qué consumidores del mensaje aún se encuentran conectados
            var conexionesConsumidoresMensaje = Estado.conexionesConsumidores.filter((conexion) => mensaje.consumidores.includes(conexion.id));

            //Si no quedan consumidores
            if (conexionesConsumidoresMensaje.length == 0) {
                //Se remueve el mensaje
                cola.removerMensaje();
                actualizarColaBack(cola.nombre);
            } else {
                //Se consume el mensaje
                consumirTodos(conexionesConsumidoresMensaje, cola, mensaje);
            }
        };
            break;
        case "R": {
            var conexionesConsumidoresMensaje = Estado.conexionesConsumidores.filter((conexion) => conexion.cola == cola.nombre);
            //Si no quedan consumidores
            if (conexionesConsumidoresMensaje.length == 0) {
                //Se vuelve y se indica que no se está consumiendo mensajes
                cola.consumiendo = false;
                return;
            } else {
                //Se consume Round Robin
                consumirRoundRobin(conexionesConsumidoresMensaje, cola, mensaje);
            }

        };
            break;
    }
};

consumirTodos = async (conexionesConsumidoresMensaje, cola, mensaje) => {

    //Se arma la lista de promcesas
    var promesas = conexionesConsumidoresMensaje.map((conexion) => new Promise((resolve, reject) => {
        console.log("Se envia mensaje al consumidor")
        funciones.emitAsync(conexion.socket, 'consumir_mensaje', mensaje.data)
            .then(() => resolve())
            .catch((error) => reject(error))
    }));

    //Se ejecutan las promesas
    Promise.all(promesas)
        .then(() => {
            //Si todas terminan bien =>  se finalizan los mensajes
            finalizarMensaje(cola)
        })
        .catch((error) => {
            if (error == "Timeout") {
                console.log("Timeout en mensaje")
                //En caso de Timeout, se asume que el mensaje se consumió correctamente
                finalizarMensaje(cola);
            } else {
                console.log("Se rompio algo");
                console.log(error);
            }

        });

};

consumirRoundRobin = async (conexionesConsumidoresMensaje, cola, mensaje) => {
    console.log("Consume Round Robin");
    var conexionConsumidor = cola.seleccionarConsumidor(conexionesConsumidoresMensaje);
    try {
        //Se emíte el mensaje y se aguarda confirmación
        await funciones.emitAsync(conexionConsumidor.socket, 'consumir_mensaje', mensaje.data);
        //Se finaliza el mensaje
        finalizarMensaje(cola);
    } catch (error) {
        if (error == "Timeout") {
            console.log("Timeout en mensaje")
            //Se vuelve a consumir el mensaje
            cola.cambiarConsumidor(conexionesConsumidoresMensaje, conexionConsumidor);
            consumirMensaje(cola);
        } else {
            console.log("Se rompio algo");
            console.log(error);
        }
    }

}

finalizarMensaje = (cola) => {
    cola.removerMensaje();
    actualizarColaBack(cola.nombre);

    if (!cola.borrada) {
        // Si la cola no se borró => se consume el mensaje
        consumirMensaje(cola);
    } else {
        // Si la cola se marcó como borrada => se borra...
        borrarCola(cola);
    };
}

module.exports.BorrarCola = (colaNombre, ack) => {
    console.log("Borrar cola: " + colaNombre);
    console.log(Estado.colas);

    //Se busca la cola
    var cola = Estado.colas.find((cola) => cola.nombre == colaNombre);

    if (cola.mensajes.length == 0) {
        //Si la cola no tiene mensajes pendientes => se borra 
        borrarCola(cola);
    } else {
        //Se marca la cola como borrada para el proceso que consume
        Estado.colas.find((cola) => cola.nombre == colaNombre).borrada = true;
    };

    //Se retorna el ack
    ack();
};


borrarCola = (cola) => {
    // Se remueve la cola del estado 
    console.log("Cola a borra: " + cola.nombre);
    Estado.RemoveCola(cola);

    // Se borran las conexion de consumidores
    Estado.conexionesConsumidores.filter((conexion) => conexion.cola == cola.nombre).forEach((conexion) => {
        conexion.socket.emit('cola_borrada', {});
        Estado.removerConexionConsumidor(conexion);
    });

    //Se borran las conexiones de productores
    Estado.conexionesProductores.filter((conexion) => conexion.cola == cola.nombre).forEach((conexion) => {
        conexion.socket.emit('cola_borrada', {});
        Estado.removerConexionProductor(conexion);
    });

    //Se emite a master el mensaje de que la cola fue borrada
    Estado.getSocketMaster().emit('cola_borrada', cola.nombre);
};

module.exports.BorrarBack = (cola, ack) => {
    console.log("Borrar back: " + cola);
    Estado.RemoveBackup(cola);
    ack();
};

actualizarColaBack = (nombreCola) => {

    //Se busca el nodo que tiene la cola
    var nodo = Estado.nodos.find((nodo) => nodo.backups.includes(nombreCola));

    //Se busca el socket con dicho nodo
    var conexion = Estado.conexionesNodos.find((conexion) => conexion.nombre == nodo.nombre);

    //Se busca la cola
    var cola = Estado.findColaByNombre(nombreCola);

    //Se envía solicitud para que cree el backup
    console.log("Se envía actualizar backup: " + cola.nombre);
    conexion.socket.emit('actualizar_bk', cola);
};

module.exports.ActivarBackup = async (colaNombre, nodoBack, ack) => {

    console.log("Activar backup de cola: " + colaNombre);

    //Se busca (o crea) la conexión con el nodo
    var conexion = await conectarConBackup(nodoBack);

    var colaBack = Estado.colasBack.find((cola) => cola.nombre == colaNombre);

    //Se emite el pedido para crear la cola
    conexion.socket.emit('actualizar_bk', colaBack);

    //Se agrega la cola en el esatdo
    Estado.addColas(colaBack);

    //Se quita la cola del backup
    Estado.RemoveBackup(colaBack);

    //En 5 segundo se intentará de relanzar los mensajes pendients
    if (colaBack.mensajes.length >= 1) {
        colaBack.removerMensaje();
        if (colaBack.mensajes.length >= 1) {
            var cantidadIntentos = 0;
            setTimeout(() => revisarConexionesPostCaidas(colaBack, cantidadIntentos), 5000);
        }
    }
    console.log("Se termina de activar el backup");
    ack();
};

revisarConexionesPostCaidas = (cola, intentos) => {
    console.log("Se revisa si se pueden relanzar los mensajes");
    console.log("Intentos: " + intentos);
    const maxIntentos = 5;
    if (intentos == maxIntentos) {
        consumirMensaje(cola);
        return
    }
    var conexionesConsumidoresId = Estado.conexionesConsumidores.map((conexion) => conexion.id);
    if (cola.consumidoresId.some((consumidorId) => !conexionesConsumidoresId.includes(consumidorId))) {
        intentos = intentos + 1;
        setTimeout(() => revisarConexionesPostCaidas(cola, intentos), 5000);
    } else {
        consumirMensaje(cola);
    };
}

module.exports.SolicitarGenerarNuevoBack = async (nombreCola, nodoBack, ack) => {

    console.log("Solicitar nuevo back up: " + nombreCola);

    if (nodoBack.nombre == datosNodo.nombre) {
        console.log("NO PUEDE ABRIR UN NODO CONSIGO MISMO")
    };

    //Se busca la información de la cola
    var cola = Estado.colas.find((cola) => cola.nombre == nombreCola);

    //Se busca (o crea) la conexión con el nodo
    var conexion = await conectarConBackup(nodoBack);

    //Se emite el pedido para crear la cola
    conexion.socket.emit('actualizar_bk', cola);

    //Se ejecuta el ack
    ack();
};

module.exports.DesonectarNodo = (socket) => {
    console.log("Desconectar nodo");

    var conexionYTipo = Estado.getConexionYTipoBySocket(socket);

    //En caso que se haya borrado la cola, la desconexión ya se había borrado
    if (!conexionYTipo) {
        return;
    }

    switch (conexionYTipo.tipo) {
        case "Consumidor":
            Estado.removerConexionConsumidor(conexionYTipo.conexion);
            break;
        case "Productor":
            Estado.removerConexionProductor(conexionYTipo.conexion);
            break;
        case "Nodo":
            Estado.removerConexionNodo(conexionYTipo.conexion)
            break;
        default:
            console.log("La conexión ya fue borrada");
            break;
    }

    if (conexionYTipo.tipo == "Consumidor" || conexionYTipo.tipo == "Productor") {
        console.log("Se remueve un consumidor de la cola");
        //Se borran los consumidores de las colas y de los mensajes de cada cola
        Estado.colas.forEach((cola) => {
            if (cola.consumidoresId.includes(conexionYTipo.conexion.id)) {
                cola.mensajes.forEach((mensaje) => {
                    Estado.RemoveConsumidorFromMensaje(mensaje, conexionYTipo.conexion.id);
                })
                Estado.RemoveConsumidorFromCola(cola, conexionYTipo.conexion.id);
                actualizarColaBack(cola.nombre);
            }
        });
    }
};