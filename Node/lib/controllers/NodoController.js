var Estado = require('../model/NodeEstado');
const Cola = require('../model/Cola');

module.exports.NuevoNodo = (socket, data) => {
    Estado.conexionesNodos.push({ socket, nombre: data.nombre, ip: data.ip, port: data.port });
}

module.exports.ActualizarListas = (nuevoEstado, ack) => {
    //Se reemplaza el estado
    Estado.replaceNodos(nuevoEstado);
    console.log("Se actualizó el estado");
    console.log(Estado.nodos);
    ack();
}

module.exports.CrearBackup = (cola, ack) => {

    //Se instancia una nueva cola para el backup
    var nuevoBack = new Cola(cola.nombre, cola.modalidad);

    //Se agrega el backup en el estado
    Estado.addColasBack(nuevoBack);

    //Confirmo que se procesó bien
    ack();
};