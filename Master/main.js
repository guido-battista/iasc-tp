var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var NodoController = require('./lib/controllers/NodoController');
var MasterController = require('./lib/controllers/MasterController');
var Estado = require('./lib/model/Estado');
var heartbeats = require('heartbeats');

var funciones = require('../Commons/functions');
var Masters = require('../Commons/masters');

var datosMaster = require('./datosMaster');

var heart = heartbeats.createHeart(1000);

//Se obtiene cual es el primer nodo Maste de la lista, el cual será el activo
var MasterActivo = Masters[0];

//Se levanta el servidor en algun puerto libre
console.log("Se pondrá servidor en escucha");
server.listen(datosMaster.port, function () {
    console.log('Servidor corriendo en http://localhost:' + server.address().port.toString());
});

//Se recibe una conexion
io.on('connection', function (socket) {
    console.log('Un cliente se ha conectado');

    socket.on('envio_ip_puerto', (data) => NodoController.recibirIpPuertoNodo(data, socket));

    socket.on('nueva_cola', (cola, ack) => NodoController.nuevaCola(cola, ack));

    socket.on('borrar_cola', (cola, callback) => NodoController.solicitarBorrarCola(cola, callback));

    socket.on('cola_borrada', (cola) => NodoController.borrarCola(cola));

    socket.on('disconnect', async () => NodoController.reasignarColas(socket));

    socket.on('master_replica_conectado', () => MasterController.recibirMaster(socket));

    socket.on('master_heartbit_conectado', () => MasterController.recibirMasterHeartbit(socket, procesarMaster));
});

procesarMaster = (MasterActivo) => {

    console.log("Se procesa el Master");

    if (MasterActivo.ip != datosMaster.ip || MasterActivo.port != datosMaster.port) {
        //Se ejecuta la lógica correspondiente al Master cuando es el activo
        procesarMasterReplica(MasterActivo);

    } else {

        //Se ejecuta la lógica correspondiente al master cuando es replica
        procesarMasterActivo(MasterActivo)

    };
}

procesarMasterReplica = async (MasterActivo) => {
    //Como este no es el Master activo, genera dos conexiones con el activo

    console.log("Se procesa el Master Replica");
    console.log(MasterActivo);

    // ===============================
    // 1 - Para actualizacion de estado
    // ===============================
    
    var ioMasterActivo = require('socket.io-client');
    var direccionMaster = 'http://' + MasterActivo.ip + ":" + MasterActivo.port.toString();
    var socketMasterActivo = ioMasterActivo.connect(direccionMaster, { 'forceNew connection': true });
    console.log("Se solicita conexion con el socket: <"+  direccionMaster + ">");

    //Cuando la conexión es exitosa, se envía datos del nodo, y se levanta el servidor
    socketMasterActivo.on('connect', () => MasterController.connectMasterActivo(socketMasterActivo));

    socketMasterActivo.on('connect_failed', function(error) {
        document.write("Sorry, there seems to be an issue with the connection socketMasterActivo!");
        console.log(error)
     });

    socketMasterActivo.on('actualizar_listas', (data, ack) => MasterController.actualizarListas(data, ack));

    // =========================
    // 2 - Para recibir heartbit
    // =========================

    //Como este no es el Master activo, genera dos conexiones con el activo
    var ioMasterActivoHeartbit = require('socket.io-client');
    var socketMasterActivoHeartbit = ioMasterActivoHeartbit.connect(direccionMaster, { 'forceNew connection': true });
    console.log("Se solicita conexion con el socket heartbit: <"+  direccionMaster + ">");

    //Cuando la conexión es exitosa, se envía datos del nodo, y se levanta el servidor
    socketMasterActivoHeartbit.on('connect', () => MasterController.connectMasterActivoHeartbit(socketMasterActivoHeartbit, procesarMaster));

    //Se recibe el heartbit
    socketMasterActivoHeartbit.on('heartbit', () => MasterController.recibirHeartbit());

    socketMasterActivoHeartbit.on('connect_failed', function(error) {
        document.write("Sorry, there seems to be an issue with the connection socketMasterActivoHeartbit!");
        console.log(error)
     });
};

procesarMasterActivo = () => {

    console.log("Se procesa el Activo");

    //Se emite el heartbit a todos los sockets master replicas conectado (el emit no espera ack)
    heart.createEvent(1, (count, last) => {
        console.log('...Se ejecuta el heartbit');
        Estado.socketsMasterHeartbit.forEach((socket) => {
            console.log("Se emite heartbit");
            socket.emit('heartbit', {});
        })
    });
};

procesarMaster(MasterActivo);
