var Estado = {
    nodos: [],
    conexionesNodos: [],
    socketsMaster: [],
    socketsMasterHeartbit: []
};

module.exports.nodos = Estado.nodos;
module.exports.conexionesNodos = Estado.conexionesNodos;
module.exports.socketsMaster = Estado.socketsMaster;
module.exports.socketsMasterHeartbit = Estado.socketsMasterHeartbit;

module.exports.addNodo = (nodo) => Estado.nodos.push(nodo);
module.exports.addConexionNodo = (conexion) => Estado.conexionesNodos.push(conexion);
module.exports.addSocketMaster = (socket) => Estado.socketsMaster.push(socket);
module.exports.addSocketMasterHeartbit = (socket) => Estado.socketsMasterHeartbit.push(socket);

module.exports.removeNodo = (nodo) => {
    var index = Estado.nodos.indexOf(nodo);
    Estado.nodos.splice(index, 1);
};

module.exports.removeConexionNodo = (conexion) => {
    var index = Estado.conexionesNodos.indexOf(conexion);
    Estado.conexionesNodos.splice(index, 1);
};

module.exports.removeSocketMaster = (socket) => {
    var index = Estado.socketsMaster.indexOf(socket);
    Estado.socketsMaster.splice(index, 1);
};

module.exports.removeSocketMasterHeartbit = (socket) => {
    var index = Estado.socketsMasterHeartbit.indexOf(socket);
    Estado.socketsMasterHeartbit.splice(index, 1);
};

module.exports.replaceNodos = (nuevosNodos) => Estado.nodos.splice(0, Estado.nodos.length, ...nuevosNodos);

module.exports.nodoAddCola = (nodo, cola) => {
    nodo.colas.push(cola);
}

module.exports.nodoAddBack = (nodo, back) => {
    nodo.backups.push(back);
}

module.exports.nodoRemoveCola = (nodo, cola) => {
    var index = nodo.colas.indexOf(cola);
    nodo.colas.splice(index, 1);
}

module.exports.nodoRemoveBack = (nodo, back) => {
    var index = nodo.backups.indexOf(back);
    nodo.backups.splice(index, 1);
};

module.exports.getNodoBackOfCola = (cola) => {
    var ret = {
        activo: null,
        back: null
    }
    Estado.nodos.forEach((nodo) => {
        console.log(nodo);
        if (nodo.colas.includes(cola)) {
            ret.activo = nodo;
        }; 
        if (nodo.backups.includes(cola)) {
            ret.back = nodo;
        }
    })
    return ret;
};

module.exports.getConexionOfNodo = (nodoNombre) => {
    return Estado.conexionesNodos.find((nodo) => nodo.nombre == nodoNombre); 
}