class Nodo {
    constructor(nombre, ip, port, colas, backups) {
        this.nombre = nombre;
        this.ip = ip;
        this.port = port;
        this.colas = colas;
        this.backups = backups
    };

};

module.exports = Nodo;