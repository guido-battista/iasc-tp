var Estado = require('../model/Estado');
var Nodo = require('../model/Nodo');
var ConexionNodo = require('../model/ConexionNodo');
var Nodo = require('../model/Nodo');
var funciones = require('../../../commons/functions');

module.exports.recibirIpPuertoNodo = (data, socket) => {
    Estado.addConexionNodo(new ConexionNodo(socket, data.nombre, data.ip, data.port));
    if (!data.reconexion) {
        Estado.addNodo(new Nodo(data.nombre, data.ip, data.port, [], []));
    }
}

module.exports.nuevaCola = async (cola, ack) => {
    console.log("Crear cola: " + cola.nombre);

    //Se definen los nodos que tendran a la cola (backup y activos)
    socketsParaNuevaCola = asignarNodos(cola);
    try {
        //Se informa el nuevo Estado al sistema
        await informarNuevoEstado();
        //Se le pide al nodo que tendrá a la cola que la cree
        await funciones.emitAsync(socketsParaNuevaCola.activo.socket, 'crear_cola', cola);
        //Se le pide al nodo que tendrá el backup que lo cree
        await funciones.emitAsync(socketsParaNuevaCola.back.socket, 'crear_backup', cola);
    } catch (error) {
        console.log("Error al crear la cola => " + error);
    };
    //Se ejecuta el callback
    ack();
}

asignarNodos = (cola) => {
    var nodoCola;
    var nodoBack;
    if (Estado.nodos.length == 1) {
        nodoCola = Estado.nodos[0];
        nodoBack = Estado.nodos[0];
    } else {
        //Se define qué nodo va a tener la cola
        nodoCola = Estado.nodos.reduce((min, nodo) => nodo.colas.length <= min.colas.length ? nodo : min, Estado.nodos[0]);

        //Se define qué nodo va a tener el backup
        nodoBack = definirNodoDeBackup(nodoCola);

        //Se actualiza la información de dichos nodas
        Estado.nodoAddCola(nodoCola, cola.nombre);
        Estado.nodoAddBack(nodoBack, cola.nombre);
    }

    return {
        activo: Estado.conexionesNodos.find((conexion) => { return conexion.nombre == nodoCola.nombre }),
        back: Estado.conexionesNodos.find((conexion) => { return conexion.nombre == nodoBack.nombre })
    };
};

informarNuevoEstado = async () => {
    //Se informa el estado a los nodos
    try {
        Estado.conexionesNodos.forEach(async (conexion) => {
            await funciones.emitAsync(conexion.socket, 'actualizar_listas', Estado.nodos);
        });
    } catch (error) {
        console.log("Error al informar el nuevo estado de los nodos => " + error);
    };

    //Se informa el estado a los masters replicas
    try {
        Estado.socketsMaster.forEach(async (socket) => {
            await funciones.emitAsync(socket, 'actualizar_listas', Estado.nodos);
        });
    } catch (error) {
        console.log("Error al informar el nuevo estado de los masters => " + error);
    };

};

definirNodoDeBackup = (nodoCola) => {
    return Estado.nodos.reduce((min, nodo) => (nodo.backups.length <= min.backups.length && nodo.nombre != nodoCola.nombre) ? nodo : min, Estado.nodos.find(function (nodo) {
        return nodo.nombre != nodoCola.nombre
    }));
};

module.exports.reasignarColas = async (socket) => {
    console.log("Se desconectó un nodo");
    try {

        //Se identifica qué nodo se desconectó
        var conexion = Estado.conexionesNodos.find((conexion) => conexion.socket == socket);

        //Se identifica el nodo que se cayó
        var nodo = Estado.nodos.find((nodo) => nodo.nombre == conexion.nombre);

        //Se quita el nodo del estado, para que no se le asignen mas colas
        Estado.removeNodo(nodo);

        //Se quita la conexión del nodo
        Estado.removeConexionNodo(conexion);

        //Se limpia el estado
        await limpiarEstado();

        //Se informan a los nodos que tenían los backups del nodo qué se cayó, que activen sus colas
        await funciones.asyncForEach((nodo.colas), async (cola) => {
            var nodoConBack = Estado.nodos.find((nodo) => nodo.backups.includes(cola));

            var conexionConNodoBack = Estado.conexionesNodos.find((conexion) => conexion.nombre == nodoConBack.nombre);

            //Se define el nodo que tendrá el nuevo backup
            var nodoBackNuevo = definirNodoDeBackup(nodoConBack);

            //Se envía pedido para activar el backup
            await funciones.emitAsync(conexionConNodoBack.socket, 'activar_back', { cola, nodoBack: nodoBackNuevo });

            //Se quita la cola de bakcups
            Estado.nodoRemoveBack(nodoConBack, cola);

            //Se agrega en colas
            Estado.nodoAddCola(nodoConBack, cola);

            //Se agrega el backup
            Estado.nodoAddBack(nodoBackNuevo, cola)

        });

        //Se definen los nuevos backups de las colas que el nodo caido guardaba como backup
        // nodo.backups.forEach(async (back) => {
        await funciones.asyncForEach((nodo.backups), async (back) => {
            console.log("Backup a restaurar: " + back);

            //Se busca el nodo que tiene la cola
            var nodoCola = Estado.nodos.find((nodo) => nodo.colas.includes(back));

            //Se define el nodo con menos backups
            var nodoBack = definirNodoDeBackup(nodoCola);

            //Se busca la conexión con el nodo que tiene la cola
            var conexionNodoCola = Estado.conexionesNodos.find((conexion) => conexion.nombre == nodoCola.nombre);

            //Se le pide al nodo que tiene la cola que envíe el backup
            await funciones.emitAsync(conexionNodoCola.socket, 'solicitar_back_up', { cola: back, nodoBack });

            //Se actualiza el estado
            Estado.nodoAddBack(nodoBack, back);
        })

        //Se informa el nuevo estado
        informarNuevoEstado();
    } catch (error) {
        console.log("Error en la restauración => " + error);
    }
};

limpiarEstado = async () => {
    try {
        //Esto se debería poder hacer mejor
        Estado.conexionesNodos.forEach(async (conexion) => {
            // await conexion.socket.emit('actualizar_listas', nodos);
            await funciones.emitAsync(conexion.socket, 'actualizar_listas', []);
        });
    } catch (error) {
        console.log("Error al limpiar el estado => " + error);
    }
};

module.exports.borrarCola = async (cola) => {
    //Se obtiene el nodo que tiene  el bacup de la cola
    var nodos = Estado.getNodoBackOfCola(cola);

    //Se obtiene la conexión con dicho nodo
    var conexionNodoBack = Estado.getConexionOfNodo(nodos.back.nombre);

    //Se remueve la cola del nodo activo en el Estado del sistema
    Estado.nodoRemoveCola(nodos.activo, cola);

    //Se informa al nodo que tiene el backup, que borre el backup
    await funciones.emitAsync(conexionNodoBack.socket, 'borrar_back', cola);

    //Se remueve el backup en el Estado del sistema
    Estado.nodoRemoveBack(nodos.back, cola);

    //Se informa el nuevo estado a todo el sistema
    await informarNuevoEstado();

    //Se ejecuta el callback
    callback(200, "OK");
};

module.exports.solicitarBorrarCola = async (cola, callback) => {
    //Se obtienen los nodos que tienen la cola (activo y backup)
    var nodos = Estado.getNodoBackOfCola(cola);

    //Se busca la conexión con el nodo activo
    var conexionNodoActivo = Estado.getConexionOfNodo(nodos.activo.nombre);

    //Se le indica al nodo que borre la cola
    await funciones.emitAsync(conexionNodoActivo.socket, 'borrar_cola', cola);

    //Se ejecuta el callback
    callback(200, "OK");
};


