var Masters = require('../../../Commons/masters');
var datosMaster = require('../../datosMaster');
var Estado = require('../model/Estado');

var heartbeats = require('heartbeats');
var heart;
var MasterActivoOK = true;

module.exports.connectMasterActivo = (socket) => {
    console.log("Conexión exitosa con Master Activo");
    socket.emit('master_replica_conectado', {});
};

module.exports.connectMasterActivoHeartbit = (socket, restaurarMaster) => {
    console.log("Conexión exitosa con Master Activo Heartbit");
    socket.emit('master_heartbit_conectado', { });

    heart = heartbeats.createHeart(1000);
    
    //Se ejecuta cada 10 segundos la verificacion de que el Master activo está ok
    heart.createEvent(3, (count, last) => {
        console.log('Verificación Master activo OK');
        if (MasterActivoOK) {
            console.log("Master OK");
            MasterActivoOK = false;
        } else {
            console.log("Master caido!!!!!!!");
            heart.kill();
            Masters.shift();
            restaurarMaster(Masters[0]);
        }
    });
};

module.exports.recibirMaster = (socket) => {
    Estado.addSocketMaster(socket);
    console.log("Socket Master recibido");
};

module.exports.recibirMasterHeartbit = (socket) => {
    console.log("Socket Master heartbit recibido");
    Estado.addSocketMasterHeartbit(socket);
};

module.exports.actualizarListas = (data, ack) => {
    console.log("Se actualiza el estado de los nodos");
    Estado.replaceNodos(data);
    console.log(Estado.nodos);
    ack();
};

module.exports.recibirHeartbit = () => {
    console.log("Se recibe el heartbit del Master activo");
    MasterActivoOK = true;
};
