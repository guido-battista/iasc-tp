var _ = require("underscore")

module.exports.asyncForEach =  async (array, callback) => {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
  }

  module.exports.emitAsync = function(io, event, payload){
    console.log("Se ejecuta emit async");
    console.log(event);
    return new Promise(function (resolve, reject) {
      var timeout = setTimeout(() => { 
        reject("Timeout") }, 10000);
      return io.emit(event, payload, function(){
        //Se limpia la revision del timeout
        clearTimeout(timeout);
        var args = _.toArray(arguments)
        if(args[0]) return reject(new Error(args[0]))
        return resolve.apply(null, args)
      })
    })
  };

  module.exports.Masters = [
    {
      Ip: 'localhost',
      Port: 8080,
      Activo: true
    },
    {
      Ip: 'localhost',
      Port: 8081,
      Activo: false,
    },
    {
      Ip: 'localhost',
      Port: 8082,
      Activo: true
    },
  ];